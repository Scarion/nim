# Function to calculate the score each turn
def turn(mainScore = 0):
    playerTurn = 0
    validEntry = False
    # Check for a valid entry
    while not validEntry:
        try:
            playerTurn = int(input(' '))
            if not playerTurn > 3:
                validEntry = True
            else:
                print("Enter a number between 1 and 3")
        except ValueError:
            print("That wasn't a number!")

    mainScore = mainScore + playerTurn
    print("Score = ", mainScore)

    return mainScore

# Function to switch the player's turn
def switchPlayerTurn(playerSwitch = False):
    if playerSwitch == True:
        playerSwitch = False
    else:
        playerSwitch = True
    return playerSwitch

def determinePlayer(playerSwitch = False):
    playerName = ""
    if playerSwitch == True:
        playerName = "Player 1"
    else:
        playerName = "Player 2"

    return playerName

print("Welcome to Nim!")
print("Enter a number between 1 and 3...")

score = 0
scoreLimit = 21
playerSwitch = True

print("Score = ", score)

# Main while loop of the game, while the main score is less than the end score
while score < scoreLimit:
    print(determinePlayer(playerSwitch))
    score = turn(score)
    playerSwitch = switchPlayerTurn(playerSwitch)

print("GAME OVER - ", determinePlayer(playerSwitch), " Wins!")
